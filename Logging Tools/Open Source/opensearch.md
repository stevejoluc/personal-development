# [The OpenSearch Project](https://www.opensearch.org/)

# What is the OpenSearch project? 

- OpenSearch is an open source search and analytics tools that allows you to utilize the Apache 2.0 license that comes from Elasticsearch 7.10.2 and Kibana 7.10.2. 

- Has a search engine daemon and provides a wide variety of plugins that provide oberservability.

## 

## Useful resources
- [Project Roadmap](https://github.com/orgs/opensearch-project/projects/1)
- [Beta Documents](https://docs-beta.opensearch.org/)
- [Downloads](https://www.opensearch.org/downloads.html)