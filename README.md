# Personal Development

## What am I currently working on?
- To view current, past, and future stories I am working on, please visit my [Development Story board](https://gitlab.com/stevejoluc/personal-development/-/boards)

## What online courses am I currently taking?
- **Udemy** - [The Python Mega Course](https://www.udemy.com/course/the-python-mega-course/)

- **Learn.cantrill.io** - [AWS Certified Solutions Architect - Associate (SAA-C02)](https://learn.cantrill.io/p/aws-certified-solutions-architect-associate-saa-c02)
